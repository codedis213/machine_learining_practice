import numpy as np
import pandas as pd
import sklearn
from sklearn.model_selection import train_test_split
from sklearn import preprocessing



def process_input(dataset_url):
    # Mapping of columns with the names
    TrainingColumns = ['fd_transac_amt', 'fd_days_since_last_transac', 'fd_account_age', 'fd_cust_age',
                       'fd_transac_amt_perc_by_avg', 'fd_transac_24hr_same_acc_same_type',
                       'fd_transac_24hr_same_acc_type_1',
                       'fd_transac_24hr_same_acc_type_2', 'fd_transac_24hr_same_acc_type_3',
                       'fd_transac_24hr_same_acc_type_4', 'fd_transac_24hr_same_acc_type_7',
                       'fd_transac_24hr_same_acc_type_11', 'fd_transac_24hr_same_acc_type_12',
                       'fd_transac_24hr_same_acc_type_15', 'fd_transac_3month_same_acc_same_type',
                       'fd_transac_3month_same_acc_type_1', 'fd_transac_3month_same_acc_type_2',
                       'fd_transac_3month_same_acc_type_3', 'fd_transac_3month_same_acc_type_4',
                       'fd_transac_3month_same_acc_type_7', 'fd_transac_3month_same_acc_type_11',
                       'fd_transac_3month_same_acc_type_12', 'fd_transac_3month_same_acc_type_15',
                       'fd_transac_amt_sum_24hr_same_acc_type_1', 'fd_transac_amt_sum_24hr_same_acc_type_2',
                       'fd_transac_amt_sum_24hr_same_acc_type_3', 'fd_transac_amt_sum_24hr_same_acc_type_4',
                       'fd_transac_amt_sum_3month_same_acc_type_1', 'fd_transac_amt_sum_3month_same_acc_type_2',
                       'fd_transac_amt_sum_3month_same_acc_type_3', 'fd_transac_amt_sum_3month_same_acc_type_4',
                       'fd_transac_24hr_same_acc_same_route', 'fd_transac_24hr_same_acc_route_1',
                       'fd_transac_24hr_same_acc_route_3', 'fd_transac_24hr_same_acc_route_4',
                       'fd_transac_24hr_same_acc_route_5', 'fd_transac_3month_same_acc_route_1',
                       'fd_transac_3month_same_acc_route_3', 'fd_transac_3month_same_acc_route_4',
                       'fd_transac_3month_same_acc_route_5', 'fd_transac_24hr_same_acc_same_finan_inst',
                       'fd_transac_amt_percentage', 'fd_transac_route', 'fd_term_finan_inst', 'fd_transac_type',
                       'fd_IC_transac', 'fd_transac_hour',
                       'fd_transac_weekday', 'fd_transac_critical']
    ResultColumns = ['fd_fraud_Y']

    # Dividing Numerical and Categorical Columns
    Numerical = ['fd_transac_amt', 'fd_days_since_last_transac', 'fd_account_age', 'fd_cust_age',
                 'fd_transac_amt_perc_by_avg', 'fd_transac_24hr_same_acc_same_type', 'fd_transac_24hr_same_acc_type_1',
                 'fd_transac_24hr_same_acc_type_2', 'fd_transac_24hr_same_acc_type_3',
                 'fd_transac_24hr_same_acc_type_4', 'fd_transac_24hr_same_acc_type_7',
                 'fd_transac_24hr_same_acc_type_11', 'fd_transac_24hr_same_acc_type_12',
                 'fd_transac_24hr_same_acc_type_15', 'fd_transac_3month_same_acc_same_type',
                 'fd_transac_3month_same_acc_type_1', 'fd_transac_3month_same_acc_type_2',
                 'fd_transac_3month_same_acc_type_3', 'fd_transac_3month_same_acc_type_4',
                 'fd_transac_3month_same_acc_type_7', 'fd_transac_3month_same_acc_type_11',
                 'fd_transac_3month_same_acc_type_12', 'fd_transac_3month_same_acc_type_15',
                 'fd_transac_amt_sum_24hr_same_acc_type_1', 'fd_transac_amt_sum_24hr_same_acc_type_2',
                 'fd_transac_amt_sum_24hr_same_acc_type_3', 'fd_transac_amt_sum_24hr_same_acc_type_4',
                 'fd_transac_amt_sum_3month_same_acc_type_1', 'fd_transac_amt_sum_3month_same_acc_type_2',
                 'fd_transac_amt_sum_3month_same_acc_type_3', 'fd_transac_amt_sum_3month_same_acc_type_4',
                 'fd_transac_24hr_same_acc_same_route', 'fd_transac_24hr_same_acc_route_1',
                 'fd_transac_24hr_same_acc_route_3', 'fd_transac_24hr_same_acc_route_4',
                 'fd_transac_24hr_same_acc_route_5', 'fd_transac_3month_same_acc_route_1',
                 'fd_transac_3month_same_acc_route_3', 'fd_transac_3month_same_acc_route_4',
                 'fd_transac_3month_same_acc_route_5', 'fd_transac_24hr_same_acc_same_finan_inst',
                 'fd_transac_amt_percentage']
    Categorical = ['fd_transac_route', 'fd_term_finan_inst', 'fd_transac_type', 'fd_IC_transac', 'fd_transac_hour',
                   'fd_transac_weekday', 'fd_transac_critical']

    # Creating dataframes for training and result columns
    Training_Df = pd.read_csv(dataset_url, delimiter=",", header=0, usecols=TrainingColumns)
    Result_Df = pd.read_csv(dataset_url, delimiter=",", header=0, usecols=ResultColumns)

    # Creating Numerical and Categorical dataframe
    Numerical_Df = Training_Df[Numerical]
    Categorical_Df = Training_Df[Categorical]

    # Preprocessing of numerical data

    scaler = preprocessing.StandardScaler(copy=True, with_mean=True, with_std=True).fit(Numerical_Df)
    Preprocessed = scaler.transform(Numerical_Df)

    Preprocessed_Df = pd.DataFrame(Preprocessed, columns=Numerical)
    # Hot one encoding of categorical data
    Encoded_Df = pd.get_dummies(Categorical_Df, sparse=False,
                                columns=Categorical)

    # Concat both numerical and categorical data for processing
    Final_Df = pd.concat([Encoded_Df, Preprocessed_Df], axis=1)

    # Stratified sampling and division of train test in ration 80:20
    X_train, X_test, Y_train, Y_test = train_test_split(Final_Df, Result_Df, test_size=0.20, shuffle=True,
                                                        stratify=Result_Df)

    return X_train, Y_train, X_test, Y_test


if __name__ == "__main__":
    process_input()
