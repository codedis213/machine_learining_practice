import numpy as np
import pandas as pd
import sklearn
from sklearn.model_selection import train_test_split


def process_input():
    # Mapping of columns with the names
    TrainingColumns = ['fd_transac_route', 'fd_term_finan_inst', 'fd_transac_type', 'fd_transac_amt', 'fd_IC_transac',
                       'fd_transac_ref_analysis_id']
    ResultColumns = ['fd_fraud_Y']

    # Dividing Numerical and Categorical Columns
    Numerical = ['fd_transac_amt']
    Categorical = ['fd_transac_route', 'fd_term_finan_inst', 'fd_transac_type']

    # Creating dataframes for training and result columns
    Training_Df = pd.read_csv("trainingdata.csv", delimiter=",", header=0, usecols=TrainingColumns)
    Result_Df = pd.read_csv("trainingdata.csv", delimiter=",", header=0, usecols=ResultColumns)

    # Creating Numerical and Categorical dataframe
    Numerical_Df = Training_Df[Numerical]
    Categorical_Df = Training_Df[Categorical]

    # Preprocessing of numerical data
    Preprocessed = sklearn.preprocessing.scale(Numerical_Df, axis=0)
    Preprocessed_Df = pd.DataFrame(Preprocessed, columns=['Numerical'])

    # Hot one encoding of categorical data
    Encoded_Df = pd.get_dummies(Categorical_Df, sparse=False,
                                columns=['fd_transac_route', 'fd_term_finan_inst', 'fd_transac_type'])

    # Concat both numerical and categorical data for processing
    Final_Df = pd.concat([Encoded_Df, Preprocessed_Df], axis=1)

    # Stratified sampling and division of train test in ration 80:20
    X_train, X_test, Y_train, Y_test = train_test_split(Final_Df, Result_Df, test_size=0.20, shuffle=True,
                                                        stratify=Result_Df)

    print (Result_Df)
    pricessd_trained_data = X_train, Y_train, X_test, Y_test
    # print (pricessd_trained_data)
    return pricessd_trained_data


if __name__ == "__main__":
    process_input()
