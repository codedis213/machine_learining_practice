# coding: utf-8

# In[6]:


import pandas as pd
import tensorflow as tf
import numpy as np

from sklearn.cross_validation import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn import metrics

# In[7]:


df = pd.read_csv("trainingdata.csv")

# In[12]:


print ("Number of Observations :: ", len(df))
# Get the first observation
print (df.head())

# In[15]:


headers = list(df.columns.values)
print ("Data set headers :: {headers}".format(headers=headers))

# In[21]:


training_features = ['fd_transac_route', 'fd_term_finan_inst', 'fd_transac_type', 'fd_transac_amt', 'fd_IC_transac',
                     'fd_transac_amt_percentage', 'fd_transac_ref_analysis_id', 'fd_transac_hour', 'fd_transac_weekday',
                     'fd_days_since_last_transac', 'fd_account_age', 'fd_cust_age', 'fd_transac_amt_perc_by_avg',
                     'fd_transac_critical', 'fd_transac_24hr_same_acc_same_type', 'fd_transac_24hr_same_acc_type_1',
                     'fd_transac_24hr_same_acc_type_2', 'fd_transac_24hr_same_acc_type_3',
                     'fd_transac_24hr_same_acc_type_4', 'fd_transac_24hr_same_acc_type_7',
                     'fd_transac_24hr_same_acc_type_11', 'fd_transac_24hr_same_acc_type_12',
                     'fd_transac_24hr_same_acc_type_15', 'fd_transac_3month_same_acc_same_type',
                     'fd_transac_3month_same_acc_type_1', 'fd_transac_3month_same_acc_type_2',
                     'fd_transac_3month_same_acc_type_3', 'fd_transac_3month_same_acc_type_4',
                     'fd_transac_3month_same_acc_type_7', 'fd_transac_3month_same_acc_type_11',
                     'fd_transac_3month_same_acc_type_12', 'fd_transac_3month_same_acc_type_15',
                     'fd_transac_amt_sum_24hr_same_acc_type_1', 'fd_transac_amt_sum_24hr_same_acc_type_2',
                     'fd_transac_amt_sum_24hr_same_acc_type_3', 'fd_transac_amt_sum_24hr_same_acc_type_4',
                     'fd_transac_amt_sum_3month_same_acc_type_1', 'fd_transac_amt_sum_3month_same_acc_type_2',
                     'fd_transac_amt_sum_3month_same_acc_type_3', 'fd_transac_amt_sum_3month_same_acc_type_4',
                     'fd_transac_24hr_same_acc_same_route', 'fd_transac_24hr_same_acc_route_1',
                     'fd_transac_24hr_same_acc_route_3', 'fd_transac_24hr_same_acc_route_4',
                     'fd_transac_24hr_same_acc_route_5', 'fd_transac_3month_same_acc_route_1',
                     'fd_transac_3month_same_acc_route_3', 'fd_transac_3month_same_acc_route_4',
                     'fd_transac_3month_same_acc_route_5', 'fd_transac_24hr_same_acc_same_finan_inst']
target = 'fd_fraud_Y'
# Train , Test data split
train_x, test_x, train_y, test_y = train_test_split(df[training_features], df[target], train_size=0.7)
print ("train_x size :: ", train_x.shape)
print ("train_y size :: ", train_y.shape)
print ("test_x size :: ", test_x.shape)
print ("test_y size :: ", test_y.shape)

# In[23]:


logistic_regression_model = LogisticRegression()
logistic_regression_model.fit(train_x, train_y)

# In[26]:


accuracy_score = logistic_regression_model.score(train_x, train_y)
print(accuracy_score)

# In[27]:


accuracy_score = logistic_regression_model.score(test_x, test_y)
print(accuracy_score)
