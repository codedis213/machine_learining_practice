#  import libs and dataset
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier

dataset = load_iris()
X = dataset.data
y = dataset.target

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)

model = DecisionTreeClassifier(criterion="entropy", max_depth=3, random_state=42)

#  decision trees are prone to overfitting thats why we remove some 
#  sub-nodes of the tree, that's called "pruning"
#  here, we control depth of the tree using max_depth attribute
#  other option for criterion is "gini"
#  random_state: just to make sure we get same results each time we 
#  run this code

model.fit(X_train, y_train)

#  test the model
model.score(X_test, y_test)

from sklearn.externals.six import StringIO
from sklearn.tree import export_graphviz
import pydotplus

dot_data = StringIO()
export_graphviz(model, out_file=dot_data)
graph = pydotplus.graph_from_dot_data(dot_data.getvalue())

graph.write_pdf("iris.pdf")

from IPython.display import Image

Image(filename="./images/tree.png")
