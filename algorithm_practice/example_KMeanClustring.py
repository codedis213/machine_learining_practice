## load the dataset 
from sklearn.datasets import load_iris

dataset = load_iris()
X = dataset.data
y = dataset.target

## precessing
#  standardize the data to make sure each feature contributes equally 
#  to the distance
from sklearn.preprocessing import StandardScaler

ss = StandardScaler()
X_processed = ss.fit_transform(X)

## split the dataset into train and test set
from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X_processed, y, test_size=0.3, random_state=42)

## fit n nearest neighbor model 
from sklearn.neighbors import KNeighborsClassifier

model = KNeighborsClassifier(n_neighbors=5, metric="minkowski", p=2)
#  p=2 for euclidian distance
model.fit(X_train, y_train)
#  output: 
#  KNeighborsClassifier(algorithm='auto', leaf_size=30, metric='minkowski',
#           metric_params=None, n_jobs=1, n_neighbors=5, p=2,
#           weights='uniform')

## evaluate 
model.score(X_test, y_test)
#  output: 1.0
