import numpy as np
import pandas as pd
# this module contains many utilities that will help us choose between models.
from sklearn.model_selection import train_test_split
# This contains utilities for scaling, transforming, and wrangling data.
from sklearn import preprocessing
# import the families of models
# such as random forests, SVM's, linear regression models, etc.
# Within each family of models, you'll get an actual model after you fit and tune its parameters to the data.
from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import LogisticRegression
# importing the tools to help us perform cross-validation.
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import GridSearchCV
# import some metrics we can use to evaluate our model performance later.
from sklearn.metrics import mean_squared_error, r2_score
# import a way to persist our model for future use.
# Joblib is an alternative to Python's pickle package, and we'll use it because it's more efficient for storing large numpy arrays.
from sklearn.externals import joblib

# Load red wine data
# dataset_url = 'http://mlr.cs.umass.edu/ml/machine-learning-databases/wine-quality/winequality-red.csv'
dataset_url = "D:\\Machine_learining_practice\\trainingdata.csv"
data = pd.read_csv(dataset_url, sep=',')
# print(data)

#  first 5 rows of data:
# print (data.head())
# print ("data.shape")
# print (data.shape)
# print ("data.describe()")
# print (data.describe())

# separate our target (y) features from our input (X) features:

y = data.fd_fraud_Y
X = data.drop('fd_fraud_Y', axis=1)

# Split data into training and test sets.
# set aside 20% of the data as a test set for evaluating our model.
# set an arbitrary "random state" (a.k.a. seed) so that we can reproduce our results.
# stratify your sample by the target variable. This will ensure your training set looks similar to your test set, making your evaluation metrics more reliable.
X_train, X_test, y_train, y_test = train_test_split(X, y,
                                                    test_size=0.2,
                                                    random_state=123,
                                                    stratify=y)

# Declare data preprocessing steps.
# standardize our features because they were on different scales.
# Standardization is the process of subtracting the means from each feature and then dividing by the feature standard deviations.
# Many algorithms assume that all features are centered around zero and have approximately the same variance.
X_train_scaled = preprocessing.scale(X_train)
# print (X_train_scaled)

# confirm that the scaled dataset is indeed centered at zero, with unit variance:
# print (X_train_scaled.mean(axis=0))
# print (X_train_scaled.std(axis=0))
#
# Fit the transformer on the training set(saving the means and standard deviations)
# the scaler object has the saved means and standard deviations for each feature in the training set.
scaler = preprocessing.StandardScaler().fit(X_train)

# Apply the transformer to the training set (scaling the training data)
X_train_scaled = scaler.transform(X_train)
# print (X_train_scaled.mean(axis=0))
# print (X_train_scaled.std(axis=0))

# Applying transformer to test dataPython
# transforming the test set using the means from the training set, not from the test set itself.
X_test_scaled = scaler.transform(X_test)

# print (X_test_scaled.mean(axis=0))
# print (X_test_scaled.std(axis=0))

# In practice, when we set up the cross-validation pipeline,
# we won't even need to manually fit the Transformer API. Instead, we'll simply declare the class object, like so:

# a modeling pipeline that first transforms the data using StandardScaler() and
# then fits a model using a random forest regressor.
# pipeline = make_pipeline(preprocessing.StandardScaler(),
#                          RandomForestRegressor(n_estimators=100))

pipeline = make_pipeline(preprocessing.StandardScaler(),
                         LogisticRegression()
                         )

# Declare hyperparameters to tune.\
# There are two types of parameters
# : model parameters and
#   hyperparameters.

# Models parameters can be learned directly from the data (i.e. regression coefficients),

# Hyperparameters express "higher-level" structural information about the model,
# and they are typically set before training the model.

# Within each decision tree, the computer can empirically (by observation)
# decide where to create branches based on either mean-squared-error (MSE) or
# mean-absolute-error (MAE).
# Therefore, the actual branch locations are model parameters.
print (pipeline.get_params())

# declare the hyperparameters we want to tune through cross-validation.
# hyperparameters = {'randomforestregressor__max_features': ['auto', 'sqrt', 'log2'],
#                    'randomforestregressor__max_depth': [None, 5, 3, 1]}

hyperparameters = {
    'logisticregression__solver':  ['newton-cg'],
}

# Tune model using a cross-validation pipeline.
# it helps in maximize model performance while reducing the chance of overfitting.

# Cross-validation is a process for  estimating the performance of a method
# for building a model by training and
# evaluating your model multiple times using the same method.

# These are the steps for CV:

# Split your data into k equal parts, or "folds" (typically k=10).
# Train your model on k-1 folds (e.g. the first 9 folds).
# Evaluate it on the remaining "hold-out" fold (e.g. the 10th fold).
# Perform steps (2) and (3) k times, each time holding out a different fold.
# Aggregate the performance across all k folds. This is your performance metric.

# How can you decide?
# For example, you can use CV to tune a random forest model,
# a linear regression model, and a k-nearest neighbors model, using only the training set.
# Then, you still have the untainted (not contaminated, polluted, or tainted.)
# test set to make your final selection between the model families!


# Here's how the CV pipeline looks after including preprocessing steps:

# Split your data into k equal parts, or "folds" (typically k=10).
# Preprocess k-1 training folds.
# Train your model on the same k-1 folds.
# Preprocess the hold-out fold using the same transformations from step (2).
# Evaluate your model on the same hold-out fold.
# Perform steps (2) - (5) k times, each time holding out a different fold.
# Aggregate the performance across all k folds. This is your performance metric.
# Fortunately, Scikit-Learn makes it stupidly simple to set this up:


# Sklearn cross-validation with pipelinePython
clf = GridSearchCV(pipeline, hyperparameters, cv=10)  # Fit and tune model
clf.fit(X_train, y_train)

print(clf)
print (clf.best_params_)
print (clf.refit)

# Evaluate model pipeline on test data.
# predict a new set of data:
y_pred = clf.predict(X_test)

# metrics to evaluate our model performance.
print (r2_score(y_test, y_pred))
print(mean_squared_error(y_test, y_pred))

# Save model for future use.
# save model to a .pkl filePython

joblib.dump(clf, 'rf_regressor2.pkl')

# When you want to load the model again, simply use this function:
clf2 = joblib.load('rf_regressor2.pkl')

# Predict data set using loaded model
clf2.predict(X_test)
